package com.zuitt.discussion.Controller;

import com.zuitt.discussion.Service.CourseService;
import com.zuitt.discussion.models.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    // Create Course
    //@PostMapping("/course")
    @RequestMapping(value = "/course",method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken,course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }

    // Get all course
    @GetMapping("/course")
    public ResponseEntity<Object> getCourse(){
        return new ResponseEntity<>(courseService.getCourse(), HttpStatus.OK);
    }

    // Delete a course
    @RequestMapping(value = "/course/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable Long courseid,@RequestHeader(value="Authorization") String stringToken){
        return courseService.deleteCourse(stringToken,courseid);
    }

    //    Update a Course
    @RequestMapping(value = "/course/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatedCourse(@PathVariable Long courseid,@RequestHeader(value="Authorization")String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseid,stringToken, course);
    }
    //  Get Course
    /*@GetMapping("/myPost")
    public ResponseEntity<Object> getMyPost(@RequestHeader(value="Authorization")String stringToken){
        return new ResponseEntity<>(courseService.getMyPosts(stringToken),HttpStatus.OK);*/
}
