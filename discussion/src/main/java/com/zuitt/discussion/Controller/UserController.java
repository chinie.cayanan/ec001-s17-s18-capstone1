package com.zuitt.discussion.Controller;

import com.zuitt.discussion.Service.CourseService;
import com.zuitt.discussion.Service.UserService;
import com.zuitt.discussion.exceptions.UserExceptions;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value="/users/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserExceptions {
        String username = body.get("username");
        if (!userService.findByUsername(username).isEmpty()) {
            throw new UserExceptions();
        }

        else {
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            User newUser = new User(username, encodedPassword);

            userService.createUser(newUser);
            return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
        }

    }

    }

