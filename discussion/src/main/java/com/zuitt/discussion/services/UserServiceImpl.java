package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    // Create user
    public void createUser(User user) {
        userRepository.save(user);
    }
    // Get users
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }
    // Find user by username
    public Optional<User> findByUsername(String username){

        return Optional.ofNullable(userRepository.findByUsername(username));

    }
}
