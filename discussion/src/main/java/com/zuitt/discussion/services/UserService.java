package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User user);
    Iterable<User> getUsers();
    Optional<User> findByUsername(String username);
}
