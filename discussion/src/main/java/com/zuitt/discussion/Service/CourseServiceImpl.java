package com.zuitt.discussion.Service;

import com.zuitt.discussion.Repositories.CourseRepository;
import com.zuitt.discussion.Repositories.UserRepository;
import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createCourse(String stringToken, Course course) {

        User enrolled = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(enrolled);
        courseRepository.save(newCourse);

    }

    //    Get All COurse
    public Iterable<Course> getCourse(){
        return courseRepository.findAll();
    }

    //    Delete COurse
    public ResponseEntity deleteCourse(String stringToken, Long id){
       /* postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);*/
        Course courseForDeleting = courseRepository.findById(id).get();

        String courseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthorName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course Deleted successfully", HttpStatus.OK);

        }
        else{
            return new ResponseEntity<>("You are not authorize to delete this post",HttpStatus.UNAUTHORIZED);

        }
    }

    //    Update COurse
    public ResponseEntity updateCourse(Long id,String stringToken, Course course){
        Course courseForUpdating = courseRepository.findById(id).get();
        String postAuthor = courseForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthor)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseRepository.save(courseForUpdating);

            return new ResponseEntity<>("Course updated successfully",HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorize to edit this course",HttpStatus.UNAUTHORIZED);
        }
    }

    /*//  Get users post
    public  Iterable<Course> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourse();
    }*/

}
