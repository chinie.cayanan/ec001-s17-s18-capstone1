package com.zuitt.discussion.Service;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    // Viewing all course
    Iterable<Course> getCourse();
    // Delete a course
    ResponseEntity deleteCourse(String stringToken, Long id);

    //Update a course
    ResponseEntity updateCourse(Long id,String stringToken, Course course);


}
